@flaky @manual
Feature: Produce some random failures

  Background: Just need it for testing

  @MCC01
  Scenario: Always pass
    Given I don't need anything special
    When I do nothing
    Then It fails with "0" percent chance

  @MCC02
  Scenario: Always pass
    Given I don't need anything special
    When I do nothing
    Then It fails with "0" percent chance

  @MCC10
  Scenario: Fail 10%
    Given I don't need anything special
    When I do nothing
    Then It fails with "10" percent chance

  @MCC101
  Scenario: Fail 10% 1
    Given I don't need anything special
    When I do nothing
    Then It fails with "10" percent chance

  @MCC102
  Scenario: Fail 10% 2
    Given I don't need anything special
    When I do nothing
    Then It fails with "10" percent chance

  @MCC103
  Scenario: Fail 10% 3
    Given I don't need anything special
    When I do nothing
    Then It fails with "10" percent chance

  @MCC104
  Scenario: Fail 10% 4
    Given I don't need anything special
    When I do nothing
    Then It fails with "10" percent chance

  @MCC105
  Scenario: Fail 10% 5
    Given I don't need anything special
    When I do nothing
    Then It fails with "10" percent chance

  @MCC106
  Scenario: Fail 10% 6
    Given I don't need anything special
    When I do nothing
    Then It fails with "10" percent chance

  @MCC25
  Scenario: Fail 25%
    Given I don't need anything special
    When I do nothing
    Then It fails with "25" percent chance

  @MCC251
  Scenario: Fail 25% 1
    Given I don't need anything special
    When I do nothing
    Then It fails with "25" percent chance

  @MCC252
  Scenario: Fail 25% 2
    Given I don't need anything special
    When I do nothing
    Then It fails with "25" percent chance

  @MCC253
  Scenario: Fail 25% 3
    Given I don't need anything special
    When I do nothing
    Then It fails with "25" percent chance

  @MCC50
  Scenario: Fail 50%
    Given I don't need anything special
    When I do nothing
    Then It fails with "50" percent chance


#  @MCC501
#  Scenario: Fail 50%
#    Given I don't need anything special
#    When I do nothing
#    Then It fails with "50" percent chance
#
#  @MCC75
#  Scenario: Fail 75%
#    Given I don't need anything special
#    When I do nothing
#    Then It fails with "75" percent chance
#
#  @MCC90
#  Scenario: Fail 90%
#    Given I don't need anything special
#    When I do nothing
#    Then It fails with "90" percent chance
#
#  @MCC100
#  Scenario: Fail 100%
#    Given I don't need anything special
#    When I do nothing
#    Then It fails with "100" percent chance


