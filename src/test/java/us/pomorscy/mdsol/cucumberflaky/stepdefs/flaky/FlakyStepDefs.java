package us.pomorscy.mdsol.cucumberflaky.stepdefs.flaky;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.Random;

public class FlakyStepDefs {
  private final Random random =  new Random(System.currentTimeMillis());

  @Given("^I don't need anything special$")
  public void someVoidFunction() {
  }

  @When("^I do nothing$")
  public void anotherVoidFunction() {
  }

  @Then("^It fails with \"([^\"]*)\" percent chance$")
  public void failWithProbability(String percentage) throws Exception {
    int threshold = Integer.parseInt(percentage);
    int randomValue = random.nextInt(100);
    if (randomValue < threshold) {
      throw new Exception("Random exception: " + randomValue + " < " + threshold);
    }
  }

  @After()
  public void embedScreenshot(Scenario scenario) {
    if (scenario.isFailed()) {
      scenario.embed(new byte[1], "image/png");
    }
  }


}
