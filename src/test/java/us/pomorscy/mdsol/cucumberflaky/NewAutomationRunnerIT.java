package us.pomorscy.mdsol.cucumberflaky;

import static us.pomorscy.mdsol.cucumberflaky.NewAutomationRunnerIT.MAIN_RUN_DIR;
import static us.pomorscy.mdsol.cucumberflaky.NewAutomationRunnerIT.MAIN_RUN_FAILURES_FILE_PATH;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/*
 * Do not include changes to this file in pull requests unless you have consulted on those changes
 * first. In particular, if you have changed which tags are run, please revert your change before
 * raising a PR.
 */
@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true, strict = true,
    format = {"pretty", "pretty:" + MAIN_RUN_DIR + "/cucumber.txt",
        "html:" + MAIN_RUN_DIR + "/html",
        "junit:" + MAIN_RUN_DIR + "/cucumber.junit.xml",
        "json:" + MAIN_RUN_DIR + "/cucumber.json",
        "usage:" + MAIN_RUN_DIR + "/usage.json",
        "rerun:" + MAIN_RUN_FAILURES_FILE_PATH},
    tags = {"@pullRequestCheck"}, features = "src/test/resources",
    glue = {"us.pomorscy.mdsol.cucumberflaky"})
public class NewAutomationRunnerIT {

  public static final String CUCUMBER_REPORTS_DIR = "target/it-cucumber-reports";
  static final String MAIN_RUN_DIR = CUCUMBER_REPORTS_DIR + "/main-run";
  public static final String MAIN_RUN_FAILURES_FILE_PATH = MAIN_RUN_DIR + "/failures.txt";

}
