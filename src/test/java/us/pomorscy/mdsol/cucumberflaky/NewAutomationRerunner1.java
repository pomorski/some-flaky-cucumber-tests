package us.pomorscy.mdsol.cucumberflaky;

import static us.pomorscy.mdsol.cucumberflaky.NewAutomationRerunner1.FIRST_RERUN_DIR;
import static us.pomorscy.mdsol.cucumberflaky.NewAutomationRerunner1.FIRST_RERUN_FAILURES_FILE_PATH;
import static us.pomorscy.mdsol.cucumberflaky.NewAutomationRunnerIT.CUCUMBER_REPORTS_DIR;
import static us.pomorscy.mdsol.cucumberflaky.NewAutomationRunnerIT.MAIN_RUN_FAILURES_FILE_PATH;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true, strict = true,
    format = {"pretty", "pretty:" + FIRST_RERUN_DIR + "/cucumber.txt",
        "html:" + FIRST_RERUN_DIR + "/html",
        "junit:" + FIRST_RERUN_DIR + "/cucumber.junit.xml",
        "json:" + FIRST_RERUN_DIR + "/cucumber.json",
        "usage:" + FIRST_RERUN_DIR + "/usage.json",
        "rerun:" + FIRST_RERUN_FAILURES_FILE_PATH},
    features = {"@" + MAIN_RUN_FAILURES_FILE_PATH},
    glue = {"us.pomorscy.mdsol.cucumberflaky"})
public class NewAutomationRerunner1 {

  static final String FIRST_RERUN_DIR = CUCUMBER_REPORTS_DIR + "/rerun1";
  static final String FIRST_RERUN_FAILURES_FILE_PATH = FIRST_RERUN_DIR + "/failures.txt";

}
