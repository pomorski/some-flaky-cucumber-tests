package us.pomorscy.mdsol.cucumberflaky;

import static us.pomorscy.mdsol.cucumberflaky.NewAutomationRerunner1.FIRST_RERUN_FAILURES_FILE_PATH;
import static us.pomorscy.mdsol.cucumberflaky.NewAutomationRerunner2.SECOND_RERUN_DIR;
import static us.pomorscy.mdsol.cucumberflaky.NewAutomationRerunner2.SECOND_RERUN_FAILURES_FILE_PATH;
import static us.pomorscy.mdsol.cucumberflaky.NewAutomationRunnerIT.CUCUMBER_REPORTS_DIR;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true, strict = true,
    format = {"pretty", "pretty:" + SECOND_RERUN_DIR + "/cucumber.txt",
        "html:" + SECOND_RERUN_DIR + "/html",
        "junit:" + SECOND_RERUN_DIR + "/cucumber.junit.xml",
        "json:" + SECOND_RERUN_DIR + "/cucumber.json",
        "usage:" + SECOND_RERUN_DIR + "/usage.json",
        "rerun:" + SECOND_RERUN_FAILURES_FILE_PATH},
    features = {"@" + FIRST_RERUN_FAILURES_FILE_PATH},
    glue = {"us.pomorscy.mdsol.cucumberflaky"})
public class NewAutomationRerunner2 {

  static final String SECOND_RERUN_DIR = CUCUMBER_REPORTS_DIR + "/rerun2";
  static final String SECOND_RERUN_FAILURES_FILE_PATH =  SECOND_RERUN_DIR + "/failures.txt";

}
